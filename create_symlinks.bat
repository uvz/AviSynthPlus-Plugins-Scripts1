@echo off

rem -------- Change defaults --------

:: Whether to include AiUpscale (https://github.com/Alexkral/AviSynthAiUpscale):
set _shaders=true


:: Whether to include crabshank's filters (https://github.com/crabshank/Avisynth-filters):
set _crabshank_filters=true


:: Whether to include LinearTransformation (https://github.com/FranceBB/LinearTransformation).
:: NOTE: LUTs must be downloaded from https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/tree/main/LinearTransformation
:: and a folder "LUTs" that contains the lut files must be located in the same folder as LinearTransformation.avsi.
set linear_transf_=true


:: Whether to include AutoOverlay (https://github.com/introspected/AutoOverlay):
set autooverlay_=true


:: Whether to include CUDA only (for example DGDecodeNV.dll) plugins:
set cuda_only_=false


:: Whether to include Dogway_pack (https://github.com/Dogway/Avisynth-Scripts):
set dogway_pack_=true


:: Whether to include RIFE (https://github.com/Asd-g/AviSynthPlus-RIFE).
:: NOTE: models must be downloaded from https://github.com/Asd-g/AviSynthPlus-RIFE/releases
:: and a folder "models" that contains the models must be located in the same folder as RIFE.dll.
set rife_=true


:: Whether to include avs-mlrt (https://github.com/Asd-g/avs-mlrt).
:: NOTE: models must be downloaded from https://github.com/Asd-g/avs-mlrt/releases
:: and a folder "models" that contains the models must be located in the same folder as mlrt_xx.dll.
set mlrt_ncnn_=true
:: NOTE: OpenVINO runtime files (openvino_dll.7z) must be downloaded from https://github.com/Asd-g/avs-mlrt/releases
:: and a folder "mlrt_ov_rt" that contains the runtime files must be located in the same folder as mlrt_ov.dll.
set mlrt_ov_=true
:: NOTE: ONNX runtime files (onnxruntime_dll.7z) must be downloaded from https://github.com/Asd-g/avs-mlrt/releases
:: and a folder "mlrt_ort_rt" that contains the runtime file must be located in the same folder as mlrt_ort.dll.
set mlrt_ort_cpu_=true
:: NOTE: ONNX runtime files (onnxruntime_dll.7z) and CUDA runtime files (cuda_dll.7z) must be downloaded from https://github.com/Asd-g/avs-mlrt/releases
:: and a folder "mlrt_ort_rt" that contains the runtime files must be located in the same folder as mlrt_ort.dll.
set mlrt_ort_cuda_=true
:: NOTE: ONNX runtime files (onnxruntime_dll.7z) and DirectML runtime files (dml_dll.7z) must be downloaded from https://github.com/Asd-g/avs-mlrt/releases
:: and a folder "mlrt_ort_rt" that contains the runtime files must be located in the same folder as mlrt_ort.dll.
set mlrt_ort_dml_=true

rem -------- -------- -------- --------


echo:
echo ****** clean ******
echo:

rem del "%~dp0plugins64+\*.avs*"
rem
rem if exist "%~dp0plugins64+\models" rd /s /q "%~dp0plugins64+\models"
rem
rem for %%i in ("%~dp0Dogway_pack\External_deps\x64\*.*") do if exist "%~dp0plugins64+\%%~nxi" del "%~dp0plugins64+\%%~nxi"
rem
rem for %%i in ("%~dp0AutoOverlay\*.dll") do if exist "%~dp0plugins64+\%%~nxi" del "%~dp0plugins64+\%%~nxi"
rem
rem if exist "%~dp0plugins64+\Shaders" rd /s /q "%~dp0plugins64+\Shaders"
rem
rem for %%i in ("%~dp0AutoOverlay\x64\*.dll") do if exist "%~dp0plugins64+\%%~nxi" del "%~dp0plugins64+\%%~nxi"
rem
rem for %%i in ("%~dp0crabshank_filters\x64\*.dll") do if exist "%~dp0plugins64+\%%~nxi" del "%~dp0plugins64+\%%~nxi"
rem
rem if exist "%~dp0plugins64+\DGDecodeNV.dll" del "%~dp0plugins64+\DGDecodeNV.dll"
rem for %%i in ("%~dp0cuda_only\x64\*.dll") do if exist "%~dp0plugins64+\%%~nxi" del "%~dp0plugins64+\%%~nxi"
rem
rem if exist "%~dp0plugins64+\fftw3.dll" del "%~dp0plugins64+\fftw3.dll"
rem
rem if exist "%~dp0plugins64+\RIFE.dll" del "%~dp0plugins64+\RIFE.dll"
rem
rem if exist "%~dp0plugins64+\mlrt_ncnn.dll" del "%~dp0plugins64+\mlrt_ncnn.dll"

cd /d "%~dp0"
git clean -fd
rem end ====================================


echo:
echo ****** create fftw3 symlinks ******
echo:

mklink "%~dp0plugins64+\fftw3.dll" "%~dp0plugins64+\libfftw3f-3.dll"
if errorlevel 1 (
    call :err
    echo Error: failed symlink creation "%~dp0plugins64+\fftw3.dll".
    goto err1
)
rem end ====================================


echo:
echo ****** create scripts symlinks ******
echo:

for %%i in ("%~dp0\scripts\*.*") do (
    mklink "%~dp0plugins64+\%%~nxi" "%%i"
    if errorlevel 1 (
        call :err
        echo Error: failed symlink creation "%%i".
        goto err1
    )
)
rem end ====================================


echo:
echo ****** create AutoOverlay symlinks ******
echo:

if "%autooverlay_%"=="true" (
    for %%i in ("%~dp0AutoOverlay\*.*") do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error: failed symlink creation "%%i".
            goto err1
        )
    )
)
rem end ====================================


echo:
echo ****** create AiUpscale symlinks ******
echo:

if "%_shaders%"=="true" (
    mklink /j "%~dp0plugins64+\Shaders" "%~dp0AviSynthAiUpscale\Shaders"
    if errorlevel 1 (
        call :err
        echo Error: failed symlink creation "%~dp0plugins64+\Shaders".
        goto err1
    )
    mklink "%~dp0plugins64+\AiUpscale.avsi" "%~dp0AviSynthAiUpscale\AiUpscale.avsi"
    if errorlevel 1 (
        call :err
        echo Error: failed symlink creation "%~dp0plugins64+\AiUpscale.avsi".
        goto err1
    )
)
rem end ====================================


echo:
echo ****** create crabshank_filters symlinks ******
echo:

if "%_crabshank_filters%"=="true" (
    for %%i in ("%~dp0crabshank_filters\x64\*.dll") do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error: failed symlink creation "%%i".
            goto err1
        )
    )
)
rem end ====================================


echo:
echo ****** create LinearTransformation symlinks ******
echo:

if "%linear_transf_%"=="true" (
    > "%~dp0plugins64+\LinearTransformation.avsi" (
        for /f "delims=" %%i in ('findstr /n "^" "%~dp0LinearTransformation\LinearTransformation.avsi"') do (
            set "line=%%i"
            setlocal enabledelayedexpansion
            set line=!line:"C:\Program Files (x86)\AviSynth+\LUTs"="%~dp0plugins64+\LUTs"!
            echo(!line:*:=!
            endlocal
        )
    )
    if errorlevel 1 (
        call :err
        echo Error: LinearTransformation.avsi lut path changing.
        goto err1
    )
)
rem end ====================================


echo:
echo ****** create cuda_only symlinks ******
echo:

if "%cuda_only_%"=="true" (
    for %%i in ("%~dp0cuda_only\x64\*.dll") do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error: failed symlink creation "%%i".
            goto err1
        )
    )
)
rem end ====================================


echo:
echo ****** create Dogway_pack symlinks ******
echo:

if "%dogway_pack_%"=="true" (
    for %%i in ("%~dp0Dogway_pack\External_deps\x64\*.*") do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error: failed symlink creation "%%i".
            goto err1
        )
    )

    for /r "%~dp0Dogway_pack" %%i in (*.avsi) do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error: failed symlink creation "%%i".
            goto failed
        )
    )
)
rem end ====================================


echo:
echo ****** create RIFE symlinks ******
echo:

if "%rife_%"=="true" (
    mklink "%~dp0plugins64+\RIFE.dll" "%~dp0rife\RIFE.dll"
    if errorlevel 1 (
        call :err
        echo Error: failed symlink creation "%~dp0plugins64+\RIFE.dll".
        goto err1
    )
)
rem end ====================================


echo:
echo ****** create mlrt symlinks ******
echo:

if "%mlrt_ncnn_%"=="true" (
    mklink "%~dp0plugins64+\mlrt_ncnn.dll" "%~dp0mlrt\x64\mlrt_ncnn.dll"
    if errorlevel 1 (
        call :err
        echo Error: failed symlink creation mlrt_ncnn.dll.
        goto err1
    )

    for %%i in ("%~dp0mlrt\scripts\mlrt*.avsi") do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error: failed symlink creation "%%i".
            goto err1
        )
    )

    set mlrt_avsi_checked=true
)

if "%mlrt_ov_%"=="true" (
    mklink "%~dp0plugins64+\mlrt_ov.dll" "%~dp0mlrt\x64\mlrt_ov.dll"
    if errorlevel 1 (
        call :err
        echo Error: failed symlink creation mlrt_ov.dll.
        goto err1
    )

    if not defined mlrt_avsi_checked (
        for %%i in ("%~dp0mlrt\scripts\mlrt*.avsi") do (
            mklink "%~dp0plugins64+\%%~nxi" "%%i"
            if errorlevel 1 (
                call :err
                echo Error: failed symlink creation "%%i".
                goto err1
            )
        )

        set mlrt_avsi_checked=true
    )
)

if "%mlrt_ort_cpu_%"=="true" (
    mklink "%~dp0plugins64+\mlrt_ort.dll" "%~dp0mlrt\x64\mlrt_ort.dll"
    if errorlevel 1 (
        call :err
        echo Error: failed symlink creation mlrt_ort.dll.
        goto err1
    )

    set mlrt_ort_checked=true

    if not defined mlrt_avsi_checked (
        for %%i in ("%~dp0mlrt\scripts\mlrt*.avsi") do (
            mklink "%~dp0plugins64+\%%~nxi" "%%i"
            if errorlevel 1 (
                call :err
                echo Error: failed symlink creation "%%i".
                goto err1
            )
        )

        set mlrt_avsi_checked=true
    )
)

if "%mlrt_ort_cuda_%"=="true" (
    if not defined mlrt_ort_checked (
        mklink "%~dp0plugins64+\mlrt_ort.dll" "%~dp0mlrt\x64\mlrt_ort.dll"
        if errorlevel 1 (
            call :err
            echo Error: failed symlink creation mlrt_ort.dll.
            goto err1
        )

        set mlrt_ort_checked=true
    )

    if not defined mlrt_avsi_checked (
        for %%i in ("%~dp0mlrt\scripts\mlrt*.avsi") do (
            mklink "%~dp0plugins64+\%%~nxi" "%%i"
            if errorlevel 1 (
                call :err
                echo Error: failed symlink creation "%%i".
                goto err1
            )
        )

        set mlrt_avsi_checked=true
    )
)

if "%mlrt_ort_dml_%"=="true" (
    if not defined mlrt_ort_checked (
        mklink "%~dp0plugins64+\mlrt_ort.dll" "%~dp0mlrt\x64\mlrt_ort.dll"
        if errorlevel 1 (
            call :err
            echo Error: failed symlink creation mlrt_ort.dll.
            goto err1
        )

        set mlrt_ort_checked=true
    )

    if not defined mlrt_avsi_checked (
        for %%i in ("%~dp0mlrt\scripts\mlrt*.avsi") do (
            mklink "%~dp0plugins64+\%%~nxi" "%%i"
            if errorlevel 1 (
                call :err
                echo Error: failed symlink creation "%%i".
                goto err1
            )
        )

        set mlrt_avsi_checked=true
    )
)
rem end ====================================


echo:
echo ****** WARNINGS ******
echo:

if "%linear_transf_%"=="true" if not exist "%~dp0plugins64+\LUTs" echo WARNING! linear_transf_ is set true ^
but there is no "LUTs" folder (must be located in the same folder as LinearTransformation.avsi). ^
(download from https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/tree/main/LinearTransformation)

if "%rife_%"=="true" if not exist "%~dp0plugins64+\models\*rife*" echo WARNING! rife_ is set true ^
but there are no rife models (folder "models" must be located in the same folder as RIFE.dll). ^
(download from https://github.com/Asd-g/AviSynthPlus-RIFE/releases)

if defined mlrt_avsi_checked if not exist "%~dp0plugins64+\models\cugan" if not exist "%~dp0plugins64+\models\dpir" ^
if not exist "%~dp0plugins64+\models\RealESRGANv2" if not exist "%~dp0plugins64+\models\safa" ^
if not exist "%~dp0plugins64+\models\waifu2x" if not exist "%_location64%\models\rife_mlrt" echo WARNING! mlrt is set true ^
but there are no mlrt models (folder "models" must be located in the same folder as mlrt_xx_.dll). ^
(download from https://github.com/Asd-g/avs-mlrt/releases)

if "%mlrt_ov_%"=="true" if not exist "%~dp0plugins64+\mlrt_ov_rt" echo WARNING! mlrt_ov_ is set true ^
but there are no OpenVINO runtime files (folder "mlrt_ov_rt" must be located in the same folder as mlrt_ov.dll). ^
(download from https://github.com/Asd-g/avs-mlrt/releases (openvino_dll.7z))

if defined mlrt_ort_checked if not exist "%~dp0plugins64+\mlrt_ort_rt" echo WARNING! mlrt_xx_ is set true ^
but there are no ONNX runtime files (folder "mlrt_ort_rt" must be located in the same folder as mlrt_ort.dll). ^
(download from https://github.com/Asd-g/avs-mlrt/releases (onnxruntime_dll.7z))

if "%mlrt_ort_cuda_%"=="true" if not exist "%~dp0plugins64+\cudart64_12.dll" echo WARNING! mlrt_ort_cuda_ is set true ^
but there are no CUDA runtime files (they must be in the "mlrt_ort_rt" folder that must be located in the same folder as mlrt_ort.dll. ^
(download from https://github.com/Asd-g/avs-mlrt/releases (cuda_dll.7z))

if "%mlrt_ort_cuda_%"=="true" if not exist "%~dp0plugins64+\DirectML.dll" echo WARNING! mlrt_ort_dml_ is set true ^
but there are no DirectML runtime file (it must be in the "mlrt_ort_rt" folder that must be located in the same folder as mlrt_ort.dll. ^
(download from https://github.com/Asd-g/avs-mlrt/releases (dml_dll.7z))
rem end ====================================


echo:
echo ****** Done ******
echo:
pause
goto :eof

:err
echo:
echo ****** ERROR! ******
echo:
goto :eof

:err1
echo:
pause
