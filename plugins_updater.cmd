@echo off

rem -------- Change defaults --------

:: The root location of the plugins that will be installed/updated:
set "_default_location=C:\Program Files (x86)\AviSynth+"


:: The name of the folder where the 64-bit plugins will be installed/updated:
set "_folder_name64=plugins64+"


:: Whether to include AiUpscale (https://github.com/Alexkral/AviSynthAiUpscale):
set _shaders=true


:: Whether to include crabshank's filters (https://github.com/crabshank/Avisynth-filters):
set _crabshank_filters=true


:: Whether to include LinearTransformation (https://github.com/FranceBB/LinearTransformation).
:: NOTE: LUTs must be downloaded from https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/tree/main/LinearTransformation
:: and a folder "LUTs" that contains the lut files must be located in the same folder as LinearTransformation.avsi.
set linear_transf_=true


:: Whether to include AutoOverlay (https://github.com/introspected/AutoOverlay):
set autooverlay_=true


:: Whether to include CUDA only (for example DGDecodeNV.dll) plugins:
set cuda_only_=false


:: Whether to include Dogway_pack (https://github.com/Dogway/Avisynth-Scripts):
set dogway_pack_=true


:: Whether to include RIFE (https://github.com/Asd-g/AviSynthPlus-RIFE).
:: NOTE: models must be downloaded from https://github.com/Asd-g/AviSynthPlus-RIFE/releases
:: and a folder "models" that contains the models must be located in the same folder as RIFE.dll.
set rife_=true


:: Whether to include avs-mlrt (https://github.com/Asd-g/avs-mlrt).
:: NOTE: models must be downloaded from https://github.com/Asd-g/avs-mlrt/releases
:: and a folder "models" that contains the models must be located in the same folder as mlrt_xx.dll.
set mlrt_ncnn_=true
:: NOTE: OpenVINO runtime files (openvino_dll.7z) must be downloaded from https://github.com/Asd-g/avs-mlrt/releases
:: and a folder "mlrt_ov_rt" that contains the runtime files must be located in the same folder as mlrt_ov.dll.
set mlrt_ov_=true
:: NOTE: ONNX runtime files (onnxruntime_dll.7z) must be downloaded from https://github.com/Asd-g/avs-mlrt/releases
:: and a folder "mlrt_ort_rt" that contains the runtime file must be located in the same folder as mlrt_ort.dll.
set mlrt_ort_cpu_=true
:: NOTE: ONNX runtime files (onnxruntime_dll.7z) and CUDA runtime files (cuda_dll.7z) must be downloaded from https://github.com/Asd-g/avs-mlrt/releases
:: and a folder "mlrt_ort_rt" that contains the runtime files must be located in the same folder as mlrt_ort.dll.
set mlrt_ort_cuda_=true
:: NOTE: ONNX runtime files (onnxruntime_dll.7z) and DirectML runtime files (dml_dll.7z) must be downloaded from https://github.com/Asd-g/avs-mlrt/releases
:: and a folder "mlrt_ort_rt" that contains the runtime files must be located in the same folder as mlrt_ort.dll.
set mlrt_ort_dml_=true

rem -------- -------- -------- --------


set "_location64=%_default_location%\%_folder_name64%"


echo:
echo ****** remove symbolic links ******
echo:

del /q /a:l "%_location64%\*.*"
for /f "tokens=*" %%i in ('dir /a:l /s /b "%_location64%"') do rd /s /q "%%i"


echo:
echo ****** clean plugins64+ parent folder ******
echo:

rem if remained from older versions
if exist "%_default_location%\nnedi3_weights.bin" del "%_default_location%\nnedi3_weights.bin"
if exist "%_default_location%\plugs" rd /s /q "%_default_location%\plugs"
if exist "%_default_location%\scripts" rd /s /q "%_default_location%\scripts"
if exist "%_default_location%\w2xncnnvk" rd /s /q "%_default_location%\w2xncnnvk"
rem end ====================================


echo:
echo ****** clean removed plugins ******
echo:

rem if remained from older versions
if exist "%_location64%\BestAudioSource.dll" del "%_location64%\BestAudioSource.dll"
rem end ====================================

echo:
echo ****** copy plugins ******
echo:

rem compare dll files
for %%i in ("%~dp0plugins64+\*.*") do (
    echo n|comp "%%i" "%_location64%\%%~nxi"
    if errorlevel 1 (
        if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
        robocopy "%~dp0plugins64+" "%_location64%" "%%~nxi"
        if errorlevel 8 (
            call :err
            echo Error: cannot copy "%%~nxi".
            goto err1
        )
    )
)
rem end ====================================


echo:
echo ****** copy scripts ******
echo:

rem compare avsi files
for %%i in ("%~dp0scripts\*.*") do (
    echo n|comp "%%i" "%_location64%\%%~nxi"
    if errorlevel 1 (
        if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
        robocopy "%~dp0scripts" "%_location64%" "%%~nxi"
        if errorlevel 8 (
            call :err
            echo Error: cannot copy "%%~nxi".
            goto err1
        )
    )
)
rem end ====================================


echo:
echo ****** AutoOverlay ******
echo:

rem if remained from older versions
if exist "%_default_location%\aoverlay" rd /s /q "%_default_location%\aoverlay"
if exist "%_default_location%\AutoOverlay" rd /s /q "%_default_location%\AutoOverlay"

if "%autooverlay_%"=="true" (
    for %%i in ("%~dp0AutoOverlay\*.*") do (
        echo n|comp "%%i" "%_location64%\%%~nxi"
        if errorlevel 1 (
            if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
            robocopy "%~dp0AutoOverlay" "%_location64%" "%%~nxi"
            if errorlevel 8 (
                call :err
                echo Error: cannot copy "%%~nxi".
                goto err1
            )
        )
    )
) else (
    for %%i in ("%~dp0AutoOverlay\*.*") do (
        if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
    )
)
rem end ====================================


echo:
echo ****** LinearTransformation ******
echo:

rem remained from older versions
if exist "%_default_location%\LinearTransformation" rd /s /q "%_default_location%\LinearTransformation"

if "%linear_transf_%"=="true" (
    > "%_location64%\LinearTransformation.avsi" (
        for /f "delims=" %%i in ('findstr /n "^" "%~dp0LinearTransformation\LinearTransformation.avsi"') do (
            set "line=%%i"
            setlocal enabledelayedexpansion
            set line=!line:"C:\Program Files (x86)\AviSynth+\LUTs"="%_default_location%\LinearTransformation\LUTs"!
            echo(!line:*:=!
            endlocal
        )
    )
    if errorlevel 1 (
        call :err
        echo Error: LinearTransformation.avsi lut path changing.
        goto :err1
    )
) else (
    if exist "%_location64%\LUTs" rd /s /q "%_location64%\LUTs"
    if exist "%_location64%\LinearTransformation.avsi" del "%_location64%\LinearTransformation.avsi"
)
rem end ====================================


echo:
echo ****** AiUpscale ******
echo:

rem if remained from old versions
if exist "%_default_location%\Shaders" rd /s /q "%_default_location%\Shaders"
if exist "%_default_location%\AviSynthAiUpscale" rd /s /q "%_default_location%\AviSynthAiUpscale"

if "%_shaders%"=="true" (
    echo n|comp "%~dp0AviSynthAiUpscale\AiUpscale.avsi" "%_location64%\AiUpscale.avsi"
    if errorlevel 1 (
        if exist "%_location64%\AiUpscale.avsi" del "%_location64%\AiUpscale.avsi"
        robocopy "%~dp0AviSynthAiUpscale" "%_location64%" "AiUpscale.avsi"
        if errorlevel 8 (
            call :err
            echo Error: cannot copy AiUpscale.avsi.
            goto err1
        )
    )

    if exist "%_location64%\Shaders" (
        dir /s "%_location64%\Shaders" | find "File(s)" > "%~dp0sh1.txt"
        dir /s "%~dp0AviSynthAiUpscale\Shaders" | find "File(s)" > "%~dp0sh2.txt"
        fc "%~dp0sh1.txt" "%~dp0sh2.txt"
        if not errorlevel 0 (
            del "%~dp0sh1.txt" "%~dp0sh2.txt"
            call :err
            echo Error: cannot compare Shaders.
            goto err1
        )
        if errorlevel 1 (
            if exist "%_location64%\Shaders" rd /s /q "%_location64%\Shaders"
            robocopy "%~dp0AviSynthAiUpscale\Shaders" "%_location64%\Shaders" /s
            if errorlevel 8 (
                call :err
                echo Error: cannot copy Shaders.
                goto err1
            )
        )
        del "%~dp0sh1.txt" "%~dp0sh2.txt"
    ) else (
        robocopy "%~dp0AviSynthAiUpscale\Shaders" "%_location64%\Shaders" /s
        if errorlevel 8 (
            call :err
            echo Error: cannot copy Shaders.
            goto err1
        )
    )
) else (
    if exist "%_location64%\Shaders" rd /s /q "%_location64%\Shaders"
    if exist "%_location64%\AiUpscale.avsi" del "%_location64%\AiUpscale.avsi"
)
rem end ====================================


echo:
echo ****** Dogway_pack ******
echo:

rem if remained from old versions
if exist "%_default_location%\Dogway_pack" rd /s /q "%_default_location%\Dogway_pack"

if "%dogway_pack_%"=="true" (
    for /r "%~dp0Dogway_pack" %%i in ("*.avsi") do (
        echo n|comp "%%i" "%_location64%\%%~nxi"
        if errorlevel 1 (
            if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
            robocopy "%%~dpi " "%_location64%" "%%~nxi"
            if errorlevel 8 (
                call :err
                echo Error: cannot copy "%%~nxi".
                goto err1
            )
        )
    )

    for %%i in ("%~dp0Dogway_pack\External_deps\x64\*.dll") do (
        echo n|comp "%%i" "%_location64%\%%~nxi"
        if errorlevel 1 (
            if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
            robocopy "%~dp0Dogway_pack\External_deps\x64" "%_location64%" "%%~nxi"
            if errorlevel 8 (
                call :err
                echo Error: cannot copy "%%~nxi".
                goto err1
            )
        )
    )
) else (
    for /r "%~dp0Dogway_pack\" %%i in ("*.avsi") do (
        if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
    )
    for %%i in ("%~dp0Dogway_pack\External_deps\x64\*.dll") do (
        if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
    )
)
rem end ====================================


echo:
echo ****** RIFE ******
echo:

rem if remained from old versions
if exist "%_default_location%\models_rife" rd /s /q "%_default_location%\models_rife"
if exist "%_default_location%\rife" rd /s /q "%_default_location%\rife"

if "%rife_%"=="true" (
    echo n|comp "%~dp0rife\RIFE.dll" "%_location64%\RIFE.dll"
    if errorlevel 1 (
        if exist "%_location64%\RIFE.dll" del "%_location64%\RIFE.dll"
        robocopy "%~dp0rife" "%_location64%" "RIFE.dll"
        if errorlevel 8 (
            call :err
            echo Error: cannot copy RIFE.dll.
            goto err1
        )
    )
) else (
    for /d %%i in ("%_location64%\models\*rife*") do if exist "%%i" rd /s /q "%%i"
    if exist "%_location64%\RIFE.dll" del "%_location64%\RIFE.dll"
)
rem end ====================================


echo:
echo ****** mlrt_ncnn ******
echo:

rem if remained from old versions
if exist "%_default_location%\mlrt" rd /s /q "%_default_location%\mlrt"
if exist "%_location64%\mlrt.avsi" del "%_location64%\mlrt.avsi"

if "%mlrt_ncnn_%"=="true" (
    echo n|comp "%~dp0mlrt\x64\mlrt_ncnn.dll" "%_location64%\mlrt_ncnn.dll"
    if errorlevel 1 (
        if exist "%_location64%\mlrt_ncnn.dll" del "%_location64%\mlrt_ncnn.dll"
        robocopy "%~dp0mlrt\x64" "%_location64%" "mlrt_ncnn.dll"
        if errorlevel 8 (
            call :err
            echo Error: cannot copy mlrt_ncnn.dll.
            goto err1
        )
    )

    for %%i in ("%~dp0mlrt\scripts\mlrt*.avsi") do (
        echo n|comp "%%i" "%_location64%\%%~nxi"
        if errorlevel 1 (
            if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
            robocopy "%~dp0scripts" "%_location64%" "%%~nxi"
            if errorlevel 8 (
                call :err
                echo Error: cannot copy "%%~nxi".
                goto err1
            )
        )
    )

    set mlrt_avsi_checked=true
) else (
    if exist "%_location64%\mlrt_ncnn.dll" del "%_location64%\mlrt_ncnn.dll"
)
rem end ====================================


echo:
echo ****** mlrt_ov ******
echo:

rem if remained from old versions
if exist "%_location64%\mlrt_ov_loader.avsi" del "%_location64%\mlrt_ov_loader.avsi"
if exist "%_location64%\openvino_dll" rd /s /q "%_location64%\openvino_dll"

if "%mlrt_ov_%"=="true" (
    echo n|comp "%~dp0mlrt\x64\mlrt_ov.dll" "%_location64%\mlrt_ov.dll"
    if errorlevel 1 (
        if exist "%_location64%\mlrt_ov.dll" del "%_location64%\mlrt_ov.dll"
        robocopy "%~dp0mlrt\x64" "%_location64%" "mlrt_ov.dll"
        if errorlevel 8 (
            call :err
            echo Error: cannot copy mlrt_ov.dll.
            goto err1
        )
    )

    if not defined mlrt_avsi_checked (
        for %%i in ("%~dp0mlrt\scripts\mlrt*.avsi") do (
            echo n|comp "%%i" "%_location64%\%%~nxi"
            if errorlevel 1 (
                if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
                robocopy "%~dp0scripts" "%_location64%" "%%~nxi"
                if errorlevel 8 (
                    call :err
                    echo Error: cannot copy "%%~nxi".
                    goto err1
                )
            )
        )

        set mlrt_avsi_checked=true
    )
) else (
    if exist "%_location64%\mlrt_ov.dll" del "%_location64%\mlrt_ov.dll"
    if exist "%_location64%\mlrt_ov_rt" rd /s /q "%_location64%\mlrt_ov_rt"
)
rem end ====================================


echo:
echo ****** mlrt_ort_cpu ******
echo:

rem if remained from old versions
if exist "%_location64%\mlrt_ort_loader.avsi" del "%_location64%\mlrt_ort_loader.avsi"
if exist "%_location64%\onnxruntime_dll" rd /s /q "%_location64%\onnxruntime_dll"

if "%mlrt_ort_cpu_%"=="true" (
    echo n|comp "%~dp0mlrt\x64\mlrt_ort.dll" "%_location64%\mlrt_ort.dll"
    if errorlevel 1 (
        if exist "%_location64%\mlrt_ort.dll" del "%_location64%\mlrt_ort.dll"
        robocopy "%~dp0mlrt\x64" "%_location64%" "mlrt_ort.dll"
        if errorlevel 8 (
            call :err
            echo Error: cannot copy mlrt_ort.dll.
            goto err1
        )
    )

    set mlrt_ort_checked=true

    if not defined mlrt_avsi_checked (
        for %%i in ("%~dp0mlrt\scripts\mlrt*.avsi") do (
            echo n|comp "%%i" "%_location64%\%%~nxi"
            if errorlevel 1 (
                if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
                robocopy "%~dp0scripts" "%_location64%" "%%~nxi"
                if errorlevel 8 (
                    call :err
                    echo Error: cannot copy "%%~nxi".
                    goto err1
                )
            )
        )

        set mlrt_avsi_checked=true
    )
)
rem end ====================================

echo:
echo ****** mlrt_ort_cuda ******
echo:

rem if remained from old versions
if exist "%_location64%\cuda_dll" rd /s /q "%_location64%\cuda_dll"

if "%mlrt_ort_cuda_%"=="true" (
    if not defined mlrt_ort_checked (
        echo n|comp "%~dp0mlrt\x64\mlrt_ort.dll" "%_location64%\mlrt_ort.dll"
        if errorlevel 1 (
            if exist "%_location64%\mlrt_ort.dll" del "%_location64%\mlrt_ort.dll"
            robocopy "%~dp0mlrt\x64" "%_location64%" "mlrt_ort.dll"
            if errorlevel 8 (
                call :err
                echo Error: cannot copy mlrt_ort.dll.
                goto err1
            )
        )

        set mlrt_ort_checked=true
    )

    if not defined mlrt_avsi_checked (
        for %%i in ("%~dp0mlrt\scripts\mlrt*.avsi") do (
            echo n|comp "%%i" "%_location64%\%%~nxi"
            if errorlevel 1 (
                if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
                robocopy "%~dp0scripts" "%_location64%" "%%~nxi"
                if errorlevel 8 (
                    call :err
                    echo Error: cannot copy "%%~nxi".
                    goto err1
                )
            )
        )

        set mlrt_avsi_checked=true
    )
)
rem end ====================================


echo:
echo ****** mlrt_ort_dml ******
echo:

rem if remained from old versions
if exist "%_location64%\directml_dll" rd /s /q "%_location64%\directml_dll"

if "%mlrt_ort_dml_%"=="true" (
    if not defined mlrt_ort_checked (
        echo n|comp "%~dp0mlrt\x64\mlrt_ort.dll" "%_location64%\mlrt_ort.dll"
        if errorlevel 1 (
            if exist "%_location64%\mlrt_ort.dll" del "%_location64%\mlrt_ort.dll"
            robocopy "%~dp0mlrt\x64" "%_location64%" "mlrt_ort.dll"
            if errorlevel 8 (
                call :err
                echo Error: cannot copy mlrt_ort.dll.
                goto err1
            )
        )

        set mlrt_ort_checked=true
    )

    if not defined mlrt_avsi_checked (
        for %%i in ("%~dp0mlrt\scripts\mlrt*.avsi") do (
            echo n|comp "%%i" "%_location64%\%%~nxi"
            if errorlevel 1 (
                if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
                robocopy "%~dp0scripts" "%_location64%" "%%~nxi"
                if errorlevel 8 (
                    call :err
                    echo Error: cannot copy "%%~nxi".
                    goto err1
                )
            )
        )

        set mlrt_avsi_checked=true
    )
)
rem end ====================================


echo:
echo ****** remove mlrt stuff if completely disabled ******
echo:

if not defined mlrt_avsi_checked (
    if exist "%_location64%\models\cugan" rd /s /q "%_location64%\models\cugan"
    if exist "%_location64%\models\dpir" rd /s /q "%_location64%\models\dpir"
    if exist "%_location64%\models\RealESRGANv2" rd /s /q "%_location64%\models\RealESRGANv2"
    if exist "%_location64%\models\safa" rd /s /q "%_location64%\models\safa"
    if exist "%_location64%\models\waifu2x" rd /s /q "%_location64%\models\waifu2x"
    if exist "%_location64%\models\rife_mlrt" rd /s /q "%_location64%\models\rife_mlrt"
    if exist "%_location64%\mlrt*.avsi" del "%_location64%\mlrt*.avsi"
)
if not defined mlrt_ort_checked (
    if exist "%_location64%\mlrt_ort_rt" rd /s /q "%_location64%\mlrt_ort_rt"
    if exist "%_location64%\mlrt_ort.dll" del "%_location64%\mlrt_ort.dll"
)
rem end ====================================


echo:
echo ****** crabshank's filters ******
echo:

rem if remained from old versions
if exist "%_default_location%\crabshank_filters" rd /s /q "%_default_location%\crabshank_filters"

if "%_crabshank_filters%"=="true" (
    for %%i in ("%~dp0crabshank_filters\x64\*.dll") do (
        echo n|comp "%%i" "%_location64%\%%~nxi"
        if errorlevel 1 (
            if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
            robocopy "%~dp0crabshank_filters\x64" "%_location64%" "%%~nxi"
            if errorlevel 8 (
                call :err
                echo Error: cannot copy "%%~nxi".
                goto err1
            )
        )
    )
) else (
    for %%i in ("%~dp0crabshank_filters\x64\*.dll") do (
        if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
    )
)
rem end ====================================


echo:
echo ****** CUDA only ******
echo:

rem if remained from old versions
if exist "%_default_location%\cuda_only" rd /s /q "%_default_location%\cuda_only"

if "%cuda_only_%"=="true" (
    for %%i in ("%~dp0cuda_only\x64\*.dll") do (
        echo n|comp "%%i" "%_location64%\%%~nxi"
        if errorlevel 1 (
            if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
            robocopy "cuda_only\x64" "%_location64%" "%%~nxi"
            if errorlevel 8 (
                call :err
                echo Error: cannot copy "%%~nxi".
                goto err1
            )
        )
    )
) else (
    for %%i in ("%~dp0cuda_only\x64\*.dll") do (
        if exist "%_location64%\%%~nxi" del "%_location64%\%%~nxi"
    )
)
rem end ====================================


echo:
echo ****** WARNINGS ******
echo:

if "%linear_transf_%"=="true" if not exist "%_location64%\LUTs" echo WARNING! linear_transf_ is set true ^
but there is no "LUTs" folder (must be located in the same folder as LinearTransformation.avsi). ^
(download from https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/tree/main/LinearTransformation)

if "%rife_%"=="true" if not exist "%_location64%\models\*rife*" echo WARNING! rife_ is set true ^
but there are no rife models (folder "models" must be located in the same folder as RIFE.dll). ^
(download from https://github.com/Asd-g/AviSynthPlus-RIFE/releases)

if defined mlrt_avsi_checked if not exist "%_location64%\models\cugan" if not exist "%_location64%\models\dpir" ^
if not exist "%_location64%\models\RealESRGANv2" if not exist "%_location64%\models\safa" ^
if not exist "%_location64%\models\waifu2x" if not exist "%_location64%\models\rife_mlrt" echo WARNING! mlrt is set true ^
but there are no mlrt models (folder "models" must be located in the same folder as mlrt_xx_.dll). ^
(download from https://github.com/Asd-g/avs-mlrt/releases)

if "%mlrt_ov_%"=="true" if not exist "%_location64%\mlrt_ov_rt" echo WARNING! mlrt_ov_ is set true ^
but there are no OpenVINO runtime files (folder "mlrt_ov_rt" must be located in the same folder as mlrt_ov.dll). ^
(download from https://github.com/Asd-g/avs-mlrt/releases (openvino_dll.7z))

if defined mlrt_ort_checked if not exist "%_location64%\mlrt_ort_rt" echo WARNING! mlrt_ort_xx_ is set true ^
but there are no ONNX runtime files (folder "mlrt_ort_rt" must be located in the same folder as mlrt_ort.dll). ^
(download from https://github.com/Asd-g/avs-mlrt/releases (onnxruntime_dll.7z))

if "%mlrt_ort_cuda_%"=="true" if not exist "%_location64%\mlrt_ort_rt\cudart64_12.dll" echo WARNING! mlrt_ort_cuda_ is set true ^
but there are no CUDA runtime files (they must be in the "mlrt_ort_rt" folder that must be located in the same folder as mlrt_ort.dll. ^
(download from https://github.com/Asd-g/avs-mlrt/releases (cuda_dll.7z))

if "%mlrt_ort_dml_%"=="true" if not exist "%_location64%\mlrt_ort_rt\DirectML.dll" echo WARNING! mlrt_ort_dml_ is set true ^
but there are no DirectML runtime file (it must be in the "mlrt_ort_rt" folder that must be located in the same folder as mlrt_ort.dll. ^
(download from https://github.com/Asd-g/avs-mlrt/releases (dml_dll.7z))
rem end ====================================


echo:
echo ****** Done ******
echo:
pause
goto :eof

:err
echo:
echo ****** ERROR! ******
echo:
goto :eof

:err1
echo:
pause
