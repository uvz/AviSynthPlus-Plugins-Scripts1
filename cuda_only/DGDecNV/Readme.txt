DGDecNV 2053
------------

1. You should install a recent nVidia video driver available for your hardware
to ensure proper operation.

2. Build 2053 no longer supports compute architecture 1.1, 1.2, and 1.3.

3. Activation of a screensaver or standby mode may interfere with
proper operation of the DGNV tools. They should be disabled during
use of the DGNV tools.

4. Proper support on laptops depends on the laptop manufacturer correctly
implementing Optimus switching and other OEM-driver-related requirements.
In some cases this support is not properly implemented by the laptop
manufacturer. For these reasons, I do not provide support for laptops.
DGDecNV may or may not work on any given laptop.

5. Use with Windows Remote Desktop is not supported. Use an alternative such as
TightVNC, etc.

6. While DGDecNV can open VOBs it does not include any code to parse IFO
files and therefore any preprocessing needed to parse multiangles, etc.,
must be done at ripping time using the capabilities of your ripping tool.

7. 4:2:2/4:4:4 chroma formats and AVC/HEVC lossless are not supported.

8. Limitations for HEVC support:

* HEVC requires an nVidia device that supports HEVC, such as the
GTX 750/950/1050 and beyond. If your device does not support HEVC and you attempt
to open an HEVC stream, a popup error message will appear. If you want to
decode HEVC on a card that supports only hybrid decoding, such as the
750/970 etc., you must enable D3D mode and DXVA mode in your INI file.
Cards that support full HW HEVC decode do not require these settings.
GTX 1050 and better is highly recommended.

* Frame/field repeats for HEVC are not yet implemented. If you have an HEVC
stream with repeat flags, please contact me.

9. Avisynth 2.6 or Avisynth+ are required.

10. If you have Windows 10 and invoke other Avisynth filters
that use the GPU together with DGDecodeNV, such as KNLMeansCL and SVP,
things will work only in a fully 64-bit environment, including DGDecNV,
Avisynth, and all other plugins. This limitation is believed to be due
to nVidia driver deficiencies in Windows 10.

11. The DGSource() source filter supports multiple instantiation, however, the number of
instantiations is limited by available memory on the nVidia video card. As always,
more is better!

(C) Copyright 2009-2021, Donald A. Graft, All Rights Reserved

