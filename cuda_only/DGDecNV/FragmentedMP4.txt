How to process fragmented MP4 files with DGDecNV
------------------------------------------------

A fragmented MP4 contains a series of segments (fragments) that
can be requested individually. This allows for transferring unbounded
streams of multimedia data, i.e., streaming. DGDecNV uses the MP4V2
library for parsing MP4 files. This library does not support fragmented
MP4 files.

Replacing the MP4V2 library in DGDecNV with code that supports fragmented MP4
files is a very large coding effort that cannot currently be justified. Neither
are many people asking for support nor do I have any personal use for it.
Also, now that DGDecNV is freeware, it is hard to justify spending lots of
development time on this feature. Instead, therefore, I provide and document
a workaround that will allow you to process fragmented MP4 files with DGDecNV.

One option is to try to convert the fragmented MP4 file to an unfragmented
MP4 file. For example, this can be achieved by using MP4Box (from GPAC) as
follows:

MP4Box -flat frag.mp4 -out unfrag.mp4

While this works fine for plain-vanilla fragmented MP4 files,
it fails for some files, e.g., for fragmented files containing DolbyVision
metadata streams (dvh1). That appears to be a deficiency of MP4Box. Accordingly,
I do not recommend this method and instead provide an MP4 demultiplexer that
works with fragmented MP4 files, including those containing DolbyVision dvh1
streams. This demultiplexer is named 'mp4demuxer' and is derived from Dolby's
program named dlb_mp4demux. It is shipped with DGDecNV under the BSD 3-clause
license. The video elementary streams demuxed by mp4demuxer can be processed
straightforwardly with DGDecNV and the audio elementary streams can be processed
by standard audio tools.

For usage details issue the command:

mp4demuxer --help

The demuxer currently supports demuxing of the following stream types:

MPEG2 video
AVC video
HEVC video (including DolbyVision metadata streams)
AC3 audio
EAC3 audio
AAC audio

MPEG and DTS audio may be added at a future time. These may be falsely detected
as AAC with the current implementation of mp4demuxer.

Note that mp4demuxer can also be used on unfragmented files as long as they
include the supported stream types.

Copyright (c) 2021 Donald A. Graft, All rights reserved.
