### Avisynth+ plugins pack

#### Content of this plugins pack - [here](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/blob/main/Filters.md)

#### Older versions [here](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts)

### Requirements

- [The latest version of this AIO pack](https://github.com/abbodi1406/vcredist/releases)
- Recent AviSynthPlus version (can be found [here](https://gitlab.com/uvz/AviSynthPlus-Builds) or [here](https://forum.doom9.org/showthread.php?t=181351))
- Some filters require additional downloads. Refer to the [create_symlinks.bat](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/blob/main/create_symlinks.bat) / [plugins_updater.bat](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/blob/main/plugins_updater.bat) settings for detailed info.

#### How to use this plugins pack for the first time

1. (optional) Make backup of the plugins folder you already have.
2. Get this repo (for example, `git clone --depth 1 https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1`)<br>
    - If the location of the cloned/downloaded repo will be used as default location for Avisynth plugins, open `create_symlinks.bat` and change the relevant settings if needed, and run the script as Administrator.
	- If the default Avisynth plugins location is different than the one of the cloned/downloaded repo (for example `"C:\Program Files (x86)\AviSynth+"`), open `plugins_updater.bat` and change the relevant settings if needed, and run the script (as Administrator if the destination location required admin privileges).
3. (optional) Use [Avisynth Info Tool](https://forum.doom9.org/showthread.php?t=176079) to check if everything is ok. There could be warning for duplicated functions - it could be safely ignored.

#### How to update this plugins pack (it's assumed that the repo is already cloned)

1. Update the files: `git pull`
2. Run again `create_symlinks.bat` / `plugins_updater.bat`.

Once in a while you can delete the cloned repo and clone it again (`git clone --depth 1 https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1`) in order to reduce the size.

#### Note

This repo is used solely for archiving and keeping various plugins in one place.
