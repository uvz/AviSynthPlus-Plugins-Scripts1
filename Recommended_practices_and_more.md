# Index

[Resizing](#resizing)\
[Matrix conversion](#matrix-conversion)\
[Dirty lines](#dirty-lines)\
[f3kdb and his sisters](#f3kdb-and-his-sisters)\
[Something about dither](#something-about-dither)\
[Tone mapping](#tone-mapping)\
[Multithreading](#multithreading)\
[1px black lines](#1px-black-lines)\
[error: clip must be frame-based](#error-clip-must-be-frame-based)\
[Comparing frames with different dimensions](#comparing-frames-with-different-dimensions)\
[ffms2](#ffms2)\
[Some handy filters](#some-handy-filters)\
[error: Cache: Filter returned invalid response to CACHE_GETCHILD_AUDIO_MODE](#error-cache-filter-returned-invalid-response-to-cache_getchild_audio_mode)

## Resizing

The internal AviSynth resizers have chroma shift when the video is `420`/`422` with `MPEG2` chroma location.

The internal `PointResize` also introduces chroma shift (different than the above ^) - [info](https://forum.doom9.org/showthread.php?p=1571315#post1571315).

There is [AutoResize](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/blob/main/scripts/AutoResize_.avsi) function that could be helpful.

[fmtconv](https://rawcdn.githack.com/EleonoreMizo/fmtconv/3eec42f8aaf86f3327b9190c6b25a0c9eca22028/doc/fmtconv.html) also can be used in replacement of the internal resizers.

If you want something fancy - deep_resize, JincResize, libplacebo_Resample, SSIM_downsample... ( https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/blob/main/Filters.md#resizers )

[Back to index](#index)

## Matrix conversion

601->709: `z_ConvertFormat(colorspace_op="601=>709", resample_filter_uv="spline36", dither_type="error_diffusion").`

If you do further conversion after the matrix conversion that includes another dither, omit dither_type="error_diffusion" for the matrix conversion.

Note that above conversion change only the matrix and keep the transfer and primaries the same.

[fmtconv](https://rawcdn.githack.com/EleonoreMizo/fmtconv/3eec42f8aaf86f3327b9190c6b25a0c9eca22028/doc/fmtconv.html) also can be used.

[Back to index](#index)

## Dirty lines

Filters that are usually used for dirty lines: FixC(R)Br, BalanceBorders, bbmod, bbmodY, Fixer, FillBorders.

About FixC(R)Br, BalanceBorders, bbmod, bbmodY, Fixer:
- FixC(R)Br changes only the brightness of the line.
- BalanceBorders, bbmod, bbmodY changes the brightness of the line but it could change the data if blur value is low.
- Fixer (EdgeFixer) uses least squares regression (similar to interpolation) and changes/creates data.

If FixC(R)Br gives good result it's always recommended using it since the original data is preserved.\
If FixC(R)Br cannot be used and BalanceBorders, bbmod, bbmodY gives good result with blur >20 it's preferable using it.\
If FixC(R)Br doesn't give good result and BalanceBorders, bbmod, bbmodY with blur >20 doesn't help too, it's preferable using Fixer.

If you want to analyze, if the source has dirty line, you can take a look at [dldet](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/main/dldet.avsi).

[Back to index](#index)

## f3kdb and his sisters

The difference between f3kdb and neo_f3kdb is:
- f3kdb has support only for stacked format (it should be consider almost depreciated) when bit depth > 8;
- neo_f3kdb has support for native high bit depth formats;
- neo_f3kdb has an additional mode (mode 4) - see [here](https://github.com/HomeOfAviSynthPlusEvolution/neo_f3kdb).

f3kdb_3 is a wrapper of neo_f3kdb. mode=4 is default. It has a edge mask (same used in gradfun3). It has additional parameters pass, lastpassgrain, lastpassmask:
- pass could be useful for severe banding. Instead of using high values, debanding could be done with lower values and applying to the same place multiply times;
- lastpassgrain and lastpassmask - whether a grain is applied for the next pass (if pass > 1), whether the edge mask is applied for the next pass (if pass > 1).

l_f3kdb_3 is a wrapper of f3kdb with the same additions as f3kdb_3 but it has some functional limitations due to the stacked format.

f3kdb_3_adg doesn't have pass, lastpassgrain, lastpassmask but it has gf, gmin, gmax. It automatically adjust the grain amount (neo_f3kdb arg "grainY") based on the frame luma average. gf, gmin, gmax control the lower and upper limits of the grain variance.

f3kgrainplus - you have to manually change "grainY" (the amount of grain). With arg "adapt" you control where to put the more grain (dark-bright parts of the frame). The function also has the ability of grain temporal stabilization.

There is also [f3kpf](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/main/f3kpf.avsi) and [lfdeband.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/main/lfdeband.avsi).

Generally using the wrappers are recommended over plain (neo_)f3kdb because they give more options for tuning (including an edge mask). mode=4 could be helpful.

If you want to go even more with tuning try something like:

```
grain = f3kdb_3_adg()
mt_merge(last, grain, agm())
```

With above example you have automatically generated grain size based on the average luma of the frame and then that grain is applied adaptively based on the dark and bright parts of the frame.

[Back to index](#index)

## Something about dither

Dither type 'error diffusion' is reducing the spatial resolution (blurring). If there are few dither "error_diffusion" conversions for a clip, it's probably better to replace them with rounding except for the last one. [Here](https://forum.doom9.org/showthread.php?p=1727107) a discussion for more info.

For example:

```
z_Spline36Resize(x, y, dither="none")
f3kdb_3_adg() # by default error_diffusion is used
```

Dither type 'ordered' is more robust and it has bigger chance to survive the encoding process. It could be visible especially if it's used more than once.

Note that you should compare how different dither types look after encoding.

The above example is suitable for cases when dither could be applied two or three times for a clip.

Another example with more filters for a clip:

```
source with bit depth < 16
z_ConvertFormat(colorspace_op="") # works internally in float, no dither by default
SmoothAdjust() # works internally in float, dither by default
dfttest() # works internally in float, no dither
bbmod # some operations are done in 16-bit, other in float, no dither
z_Resize # works internally in 16-bit, no dither by default
neo_f3kdb() # works internally in 16-bit, error_diffusion by default
```

For this example you have:
- line 2: input_depth -> float -> input_depth
- line 3: input_depth -> float -> input_depth
- line 4: input_depth -> float -> input_depth
- line 5: input_depth -> 16-bit -> float -> input_depth
- line 6: input_depth -> 16-bit -> input_depth
- line 7: input_depth -> 16-bit -> input_depth

The issue is that if you do higher_bit_depth -> lower_bit_depth several times without dither you're propagating the rounding error and the chance to see unwanted artifacts is big (mostly if input_depth is 8-bit). If you do these conversions with dither, the chance it (dither) could to be visible is big (in addition to the mentioned blur effect). The best you should do in such cases is to convert the source to 16-bit and as final step to convert back to the input_depth with error_diffusion.

For converting lower_bit_depth -> higher_bit_depth dither does nothing.

[Back to index](#index)

## Tone mapping

Some of the existing plugins for tone mapping have options to choose the precision of the intermediate results. Always use float precision when it's possible.

If you want to keep as much details as possible regardless if HDR look after tone mapping would be preserved give a try of [Tonemapper](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/main/Tonemapper.avsi). It performs dynamic tone mapping by analysing the frame data.

If you want to keep as much the HDR look as close to the one when it's graded give a try of [bt2390_pq](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/main/bt2390_pq.avsi).

[Back to index](#index)

## Multithreading

AviSynth+ by default is single threaded.

Read carefully the information about the [Multithreading functions](https://avisynthplus.readthedocs.io/en/latest/avisynthdoc/syntax/syntax_internal_functions_multithreading_new.html). If they are misused, the speed penalty can be big even compared to single thread script. Take a note of the examples.

Some plugins have internal multithreading (jpsdr plugins, dfttest, fttw filters, mvtools, (neo_)f3kdb...). Using some of these plugins together in one script could result to get huge threads number that could cause overall speed penalty or even script freezing. Note that some complex filtering scripts like QTGMC/SMDegrain internally are using some of these plugins, so if option for disabling multithreading is available, you should use it (SMDegrain(threads=1) for example). It's recommended to set these plugins to use one thread. Multithreading should be activated at AviSynth stage.

To find a optimal value of `prefetch` for a given script start from `2` and increase by `2`. Do not start from the max CPU threads. In the most cases you wouldn't have benefit if more than `CPU threads / 2` is used.

Do not use `prefetch(x)` after the source filter!

If there is `MT_SERIALIZED` plugin in the script, it's recommended to use `prefetch(x)` right after every filter except this filter (and source filter) and `prefetch(1)` right after this  filter. For example:

```
source
vsTBilateral() # MT_MULTI_INSTANCE
prefetch(x)
hqdn3d() # MT_SERIALIZED
prefetch(1)
vsTTempSmooth() # MT_MULTI_INSTANCE
prefetch(x)
```

If a lot/complex filters are used, use [SetMemoryMax](http://avisynth.nl/index.php/Internal_functions/SetMemoryMax#SetMemoryMax).

If you want to additionally improve/fine tune multithreading you can try with [this template script](https://pastebin.com/8NmieXe7).

[Back to index](#index)

## 1px black lines

Here it's assumed YUV video with 4:2:0 chroma subsampling.

When there is 1px black line on top and bottom and/or on both sides, the options are (assume 1920x1080 video has 1px black line on top and bottom):
1. `FillMargins(top=1, bottom=1).Resize(1920, 1078, 0, 1, 0, -1)`
2. `uv = FillMargins(top=1, bottom=1).Resize(1920, 1078, 0, 1, 0, -1)`\
`CombinePlanes(Crop(ExtractY(), 0, 1, 0, -1), ExtractU(uv), ExtractV(uv), "yuv", pixel_type=PixelType())`
3. `FillMargins(top=1, bottom=1)`
4. `Crop(0, 2, 0, -2)`

So:
- number `4` should be strongly considered when the video and especially the border areas are already quite blurred.
- the difference between number `1` and `2` is how luma is cropped. For number `1` luma is cropped with resizer which mean borders are interpolated (only the borders) while number `2` gives hard boundary. Technically number `2` is the proper one when the choice is between both but usually in practice the result will be 99% of the cases identical between these.
- number `1` and `2` resample chroma.
- number `3` duplicates the neighbour lines so there is no chroma resampling.
- when the choice is between number `3` and number `1`/`2` there is no right/wrong choice. It's subjective if you're prefering to resample whole chroma for just one line on top and bottom/both sides or you're prefering to duplicate one line on top and bottom/both sides.

In case you will use number `1`/`2`, chroma could be resampled in more sophisticated way (assume 1920x1080 8-bit video with 1px black line on top and bottom):

```
source=StackVertical(Crop(0, 0, 0, 2).Tweak(sat=1.5), Crop(0, 2, 0, 0))
ExtractU(source)
ConvertBits(16)
nnedi3(0, true, nsize=4, nns=2, qual=2, pscrn=0, threads=1)
z_LanczosResize(960, 539, 0, 1.5, 960, 1078, taps=6)
u=ConvertBits(8, dither=1)
ExtractV(source)
ConvertBits(16)
nnedi3(0, true, nsize=4, nns=2, qual=2, pscrn=0, threads=1)
z_LanczosResize(960, 539, 0, 1.5, 960, 1078, taps=6)
v=ConvertBits(8, dither=1)
CombinePlanes(Crop(ExtractY(source), 0, 1, 0, -1), u, v, "yuv", pixel_type=PixelType(source))
```

Note that by default FillBorders/FillMargins(left/top/right/bottom=1) will only affect luma plane for YUV 4:2:0 sources, will affect luma and chroma top/bottom for YUV 4:2:2 sources and will affect luma and chroma for RGB/YUV 4:4:4 sources. With [FillBorders (1.4.0 and later)](https://github.com/Asd-g/AviSynth-FillBorders) different values for every plane on each side can be used - for example, FillBorders(left=[1,1], right=[1,1]) will affect all planes (luma, chroma, alpha) for YUV 4:2:0 sources.

[Back to index](#index)

## error: clip must be frame-based

Some filters can throw this (or similar) error. The reason for the error is that the video is field based (interlaced) and there is frame property `_FieldBased > 0`. You can check that by writing `propShow()` after ffvideosource/lwlibavvideosource/d2vsource.

If your video is progressive but encoded as interlaced then it's reported as interlaced (`_FieldBased > 0`). In this case you can either delete frame property `_FieldBased` by adding `propDelete("_FieldBased")` right after source loading or change the property value by adding `propSet("_FieldBased", 0)` right after source loading.

If you have interlaced video you can consider to deinterlace it and then to apply any filter.

[Back to index](#index)

## Comparing frames with different dimensions

If you have to compare frames with different dimensions, an easy and good quality solution is to use [AutoOverlay](https://github.com/introspected/AutoOverlay).

For example:

```
source=ImageSource("1.png") #ffvideosource("1.mkv") # 1920x954
base=ImageSource("2.png") #ffvideosource("2.mkv") # 1920x962
OverlayEngine(base, source)
OverlayRender(base, source, gradient=20, width=1920, height=962) # source with dimension 1920x962
```

[Back to index](#index)

## ffms2

If you use FFVideoSource/FFMS2 as a source filter and you have always/occasionally 1 frame off issue and/or other issue, you can  try following:
- it's highly recommended to use [the patched version](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/blob/main/plugins64%2B/ffms2.dll) instead of [the official one](https://github.com/FFMS/ffms2/releases).
- always use mkv container with FFVideoSource/FFMS2. Do not expect a reliably result with FFVideoSource/FFMS2 and container other than mkv;
- don't use eac3to to demux the video stream directly into mkv. It's using old Haali muxer. If you insist to use eac3to for demuxing use the raw container (.h264);
- you could demux the video stream directly into mkv by using mkvtoolnix(mkvmerge). Use always a recent mkvtoolnix(mkvmerge) version;
- you can set `threads=1` in the script - `FFVideoSource/FFMS2(...., threads=1)`;
- for encoding you can use `threads=1` (this is giving enough high fps and will not be bottleneck) or even better `seekmode=0` (linear decoding) (especially if you have `prefetch(x)` in the avs script).

[Back to index](#index)

## Some handy filters

- Displaying picture type\
    [LWLInfo](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/main/LWLInfo.avsi) can be used for any source filter that supports frame properties (FFMS2/LSMASHSource/D2VSource...).\
    Modern replacement of FFInfo.\
    If a frame property isn't supported by the source filter or info is not available, automatically that frame property is ignored.

- Saving images\
    [ComparisonGen](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/main/ComparisonGen.avsi) is helpful function that will automatically save images. It supports up to 5 clips.

- [Banding detection](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/tree/master/band_det)

- [Detection for red and blue scenes, and for scene exposure](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/tree/master/sc_stats_det)

- [UpscaleCheck](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/blob/main/scripts/UpscaleCheck.avsi)

- [HDR10_Content_Metadata_chunks](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/tree/master/HDR10_Content_Metadata_chunks)

- [getnative](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1/-/tree/master/getnative)

[Back to index](#index)

## error: Cache: Filter returned invalid response to CACHE_GETCHILD_AUDIO_MODE

After AviSynth+ r3950 the audio cache is reimplemented. Plugins that aren't updated to 2.6 API will likely throw such error "Cache: Filter returned invalid response to CACHE_GETCHILD_AUDIO_MODE...".\
One example is dither.dll (includes gradfun3). There is updated dither.dll in this plugins pack.

Note: AviSynth+ >= r4011 is fixed and this error shouldn't occur anymore.

[Back to index](#index)
